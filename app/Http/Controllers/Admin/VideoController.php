<?php

namespace App\Http\Controllers\Admin;

use App\Video;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $videos=Video::with(['image'])->orderby('is_featured','desc')->get();
        return view('backend.videos.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ImageHelper $file)
    {
        //
        request()->validate([
            'title'=>'required|max:120',
            'subtitle'=>'required|max:120',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
            'video_url'=>'required'
        ]);
        $arr=array_merge($request->only(['subtitle','title']),['url'=>$request->video_url]);

        $img=$file->prepareStoreImage($request,'video');

        $video=Video::create($arr);
        $video->image()->save($img);
        return redirect('/admin/videos')->with('success','Successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $video=Video::with(['image'])->findOrFail($id);
        return view('backend.videos.edit',compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ImageHelper $file)
    {
        //
        $video=Video::findOrFail($id);
        request()->validate([
            'title'=>'required|max:120',
            'subtitle'=>'required|max:120',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
            'video_url'=>'required'
        ]);

        $arr=array_merge($request->only(['subtitle','title']),['url'=>$request->video_url]);

        $img=$file->prepareUpdateImage($request,$video,'video','video');

        $video->update($arr);
        $video->image()->update($img);
        return redirect('/admin/videos')->with('success','Successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ImageHelper $file)
    {
        //
        $video=Video::with(['image'])->findOrFail($id);
        
        if($video->type==0){
            $file->deleteImage('video',$video['image']['image_name']);
        }
        $video->image()->delete();
        $video->delete();

        return redirect('/admin/videos')->with('success','Successfully deleted');
    }

    public function featured($id)
    {
        $video=Video::findOrFail($id);
        if($video->is_featured==1){
            $video->update(['is_featured'=>0]);
            return redirect('admin/videos')->with('success','Successfully updated.');
        }else{
            $video->update(['is_featured'=>1]);
            return redirect('admin/videos')->with('success','Successfully updated.');
        }
    }
}
