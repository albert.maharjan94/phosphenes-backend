@extends('layouts.frontend.app')
@section('home','active')
@push('styles')
<style>
  /* slider */
  /* jssor slider loading skin spin css */
  .jssorl-009-spin img {
      animation-name: jssorl-009-spin;
      animation-duration: 1.6s;
      animation-iteration-count: infinite;
      animation-timing-function: linear;
  }

  @keyframes jssorl-009-spin {
      from {
          transform: rotate(0deg);
      }

      to {
          transform: rotate(360deg);
      }
  }


    /*jssor slider bullet skin 132 css*/
  .jssorb132 {position:absolute;}
  .jssorb132 .i {position:absolute;cursor:pointer;}
  .jssorb132 .i .b {fill:#fff;fill-opacity:0.8;stroke:#000;stroke-width:1600;stroke-miterlimit:10;stroke-opacity:0.7;}
  .jssorb132 .i:hover .b {fill:#000;fill-opacity:.7;stroke:#fff;stroke-width:2000;stroke-opacity:0.8;}
  .jssorb132 .iav .b {fill:#000;stroke:#fff;stroke-width:2400;fill-opacity:0.8;stroke-opacity:1;}
  .jssorb132 .i.idn {opacity:0.3;}

  .jssora051 {display:block;position:absolute;cursor:pointer;}
  .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
  .jssora051:hover {opacity:.8;}
  .jssora051.jssora051dn {opacity:.5;}
  .jssora051.jssora051ds {opacity:.3;pointer-events:none;}

  /* slider ends */

  .intro {
      height: auto !important;
      min-height: auto;
  }
  @media only screen and (max-width:990px) {
    #navbar-muziq ul li a {
      text-transform: capitalize;
      font-size: 30px;
      padding: 20px;
    }
    .title {
      font-size: 60px;
      line-height: 60px;
    }
    .pretitle {
      font-size: 40px;
      line-height: 40px;
    }
    .featured-artists .gallery-cell {
      height: 1100px;
    }
    .featured-artist .rollover .title-artist {
      font-size: 45px;
      line-height: 45px;
      margin-bottom: 10px;
    }
    .featured-artist .rollover p {
      color: #fff;
      font-size: 32px;
      line-height: 32px;
    }
  }
  .title{
    color:#fff;
  }
  .small.music{
      min-height: 352px!important;
      height: 325px !important;
  }
  .small.music img{
      height: 100% !important;
      object-fit: cover;
  }
  .video-image{
    height: 420px;
    width: 420px ;
    object-fit: cover;
  }
  .banner{
    padding:0px;
  }
  .biography{
    background-size: auto !important;
  }
  .title-edit{
    font-size: 1.2rem !important;
    height: 4rem !important;
    line-height: 1.8rem !important;
  }
  .upevents{
   text-shadow: 1px 1px 1px #000000;
   background
  }
  .upcomming-events .upevent:hover .bg-image {
     opacity: .6;
  }
  .bg-image{
    opacity: .8;
  }
  .posted{
    text-shadow: none;
  }
  .thumbnail .rollover {
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      background-color: rgba(190,203,207, .9);
      opacity: 0;
      -webkit-transition: opacity .25s ease-in-out;
      -moz-transition: opacity .25s ease-in-out;
      -ms-transition: opacity .25s ease-in-out;
      -o-transition: opacity .25s ease-in-out;
      transition: opacity .25s ease-in-out;
  }
  .thumbnail .rollover i {
      display: block;
      border: 0;
      text-indent: -999em;
      overflow: hidden;
      direction: ltr;
      width: 80px;
      height: 80px;
      background-color: transparent;
      background-repeat: no-repeat;
      background-position: bottom center;
      background-image: url('{{ asset("/assets/frontend/images/play-plus.png") }}');
      background-size: auto;
      position: absolute;
      text-align: center;
      left: 50%;
      margin-left: -40px;
      top: 100%;
      opacity: 0;
      transform: translateY(-50%);
      transition: all .5s ease 0s;
      -webkit-transition: all .5s ease;
      -ms-transition: all .5s ease;
      -o-transition: all .5s ease;
      -moz-transition: all .5s ease;
  }
  .thumbnail:hover .rollover, .thumbnail:hover .rollover i {
      opacity: 1;
  }
  .thumbnail:hover .rollover i {
      top: 50%;
  }
  .thumbnail{
    padding: 1px;
  }
  .loaded{
    width: 100% !important;
  }
  .gallery-title .title, .gallery-title .pretitle {
    color: black !important;
  }
</style>
@endpush
@section('content')


  <!-- INTRO -->
  <section class="intro full-width jIntro" id="home">
    <div class="container">
      <div class="row">
        <div class="col-md-12 banner">
          <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1600px;height:560px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1600px;height:560px;overflow:hidden;">
              @foreach($banners as $key=>$banner)
                  {{-- <img src="{{ $banner['image']['image_url'] }}" alt="slide{{ $key }}"> --}}
                  <div style="background-color:#d3890e;">
                      <img data-u="image" style="opacity:0.8;object-fit:cover !important;" data-src="{{ $banner['image']['image_url'] }}" />
                  </div>
              @endforeach
            </div>
            <a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">slider html</a>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb132" style="position:absolute;bottom:24px;right:16px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:12px;height:12px;">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
        </div>
          {{-- <div class="slider-intro">
            <div id="slides">
              <div class="overlay"></div>
              @if($banners->count()>1)
              <div class="slides-container">
              @endif
                @foreach($banners as $key=>$banner)
                  <img src="{{ $banner['image']['image_url'] }}" alt="slide{{ $key }}">
                @endforeach
              @if($banners->count()<=1)
              </div>
              @endif
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </section>

  <!-- PLAYER -->
  <!-- <div class="player horizontal">
      <div class="container">
        <div class="info-album-player">
          <div class="album-cover" id="bg-image3"></div>
          <p class="album-title">Spectrum</p>
          <p class="artist-name">Album by Blummsday</p>
        </div>
        <div class="player-content">
          <audio preload></audio>
          <ol class="playlist">
            <li><a href="#" data-src="mp3/01Electronica_Music_by_Igormaykop951_preview.mp3">Destiny</a></li>
            <li><a href="#" data-src="mp3/02Indie_Rock_Music_by_OctoSound_preview.mp3">Sunless</a></li>
            <li><a href="#" data-src="mp3/03preview_mp3.mp3">Deep inspire</a></li>
          </ol>
          <div class="nextprev">
            <span class="prev">prev</span>
            <span class="next">next</span>
          </div>
          <span class="btnloop">loop</span>
        </div>
      </div>
    </div>
     -->
  <!-- UPCOMING EVENTS -->
  <section class="section upcomming-events" id="events">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
          <div class="separator-icon">
            <i class="fa fa-ticket"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">Get ready for Indie 2020</p>
          <div class="voffset20"></div>
          <h2 class="title">upcoming events</h2>
          <div class="voffset80"></div>
          @if($events->isEmpty())
          <div class="voffset30"></div>
          <p class="pretitle">Currently no events available</p>
          <div class="voffset80"></div>
          @endif
        </div>
      </div>
      <div class="row">
        @if(!$events->isEmpty())
        <div class="col-md-12">
            <div class="upevents" style="height: 681.6px !important;">
                @foreach($events as $event)
                    <div class="upevent">
                        <div class="contain">
                            <div class="bg-image" style="background-image: url('{{ $event['image'] }}')"></div>
                            <div class="content">
                            <div class="voffset80"></div>
                            <div class="date">@if($event['date']->m) {{ $event['date']->m }} <span>m</span> @endif{{ $event['date']->d }}<span>d</span> {{ $event['date']->h }}<span>h</span> {{ $event['date']->i }}<span>m</span></div>
                            <div class="separator tag"><span>featured</span></div>
                            <div class="title title-edit">{{ $event['title'] }}</div>
                            <p>{{ $event['description'] }}</p>
                            <p class="buttons">
                                <!-- <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a> -->
                                <a href="/events/{{ $event['slug'] }}" class="btn rounded border">View details</a>
                            </p>
                            <div class="voffset70"></div>
                            <div class="posted"><span>posted:</span> {{ $event['created_at'] }}</div>
                        </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
            
            @endif
      </div>
    </div>
    </div>
  </section>
  <!-- BAND MEMBERS -->
  <section class="section featured-artists" id="members">
    <div id="fetured-artists"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
          <div class="separator-icon">
            <i class="fa fa-microphone"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">The Phosphenes Band</p>
          <div class="voffset20"></div>
          <h2 class="title">Band Members</h2>
          <div class="voffset80"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="voffset20"></div>
          <!-- <div class="js-flickity" data-flickity-options="{ &quot;cellAlign&quot;: &quot;left&quot;, &quot;wrapAround&quot;: true, &quot;contain&quot;: true, &quot;prevNextButtons&quot;: false }"> -->
          <div class="gallery-cell col-xs-12 col-sm-12 col-md-4 col-lg-3">
            <div class="featured-artist">
              <div class="image">
                <img src="{{ asset('assets/frontend/images/demo/intro/artists/abhishek.jpg') }}" alt="">
              </div>
              <div class="rollover">
                <ul class="social">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <div class="text">
                  <h4 class="title-artist">Abhishek <br>Pokhrel</h4>
                  <p>Kathmandu based singer-songwriter who started producing and sharing his music through Soundcloud
                    and YouTube back in 2014. Abhishek is a co-founder of the band and also plays guitar for Phosphenes.
                    He has successfully produced most of the bands released and upcoming originals.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="gallery-cell col-xs-12 col-sm-12 col-md-4 col-lg-3">
            <div class="featured-artist">
              <div class="image">
                <img src="{{ asset('assets/frontend/images/demo/intro/artists/aman.jpg') }}" alt="">
              </div>
              <div class="rollover">
                <ul class="social">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <div class="text">
                  <h4 class="title-artist">Aman <br>Karna</h4>
                  <p>A music producer and sound designer whose career began when he first learned to play the guitar in
                    2007. In addition to the guitar, Aman is now fluent in bass, keys, synth, and drums/ percussions.
                    From playing with an alternative rock and metal band until 2010, Aman is now a guitarist and
                    producer for Phosphenes and also a sound designer for a Kathmandu-based multimedia production
                    company, Jazz Productions.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="gallery-cell col-xs-12 col-sm-12 col-md-4 col-lg-3">
            <div class="featured-artist">
              <div class="image">
                <img src="{{ asset('assets/frontend/images/demo/intro/artists/prajwal.jpg') }}" alt="">
              </div>
              <div class="rollover">
                <ul class="social">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <div class="text">
                  <h4 class="title-artist">Prajwal <br>Aryal</h4>
                  <p>Prajwal Aryal is a Kathmandu based singer songwriter and music producer for Phosphenes. Influenced
                    majorly at a young age by rock and roll music, The Beatles is one band he has always idolized. His
                    first EP was released back in 2010 followed by the Phosphenes EP in 2016.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="gallery-cell col-xs-12 col-sm-12 col-md-4 col-lg-3">
            <div class="featured-artist">
              <div class="image">
                <img src="{{ asset('assets/frontend/images/demo/intro/artists/imogen.jpg') }}" alt="">
              </div>
              <div class="rollover">
                <ul class="social">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <div class="text">
                  <h4 class="title-artist">Imogen <br>Harper</h4>
                  <p>An Australian born singer-songwriter who plays piano and guitar for Phosphenes. Imogen has been
                    writing music since she was 15, and released a 6-song EP in 2011. Moving to Nepal in 2016 to work
                    for an international NGO, and has since performed as part of SoFar Sounds Kathmandu, Seashells on
                    the Mountains Festival, and a sold-out headline show at HUB. She is also the newest member of the
                    band Phosphenes.</p>
                </div>
              </div>
            </div>
          </div>
          <!-- </div> -->
        </div>
      </div>
      <div class="voffset120"></div>
    </div>
  </section>

  <!-- BIOGRAPHY -->
  <section class="section biography inverse-color" id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
          <div class="separator-icon">
            <i class="fa fa-microphone"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">About the band</p>
          <div class="voffset20"></div>
          <h2 class="title">biography</h2>
          <div class="voffset110"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <img src="{{ asset('assets/frontend/images/demo/intro/phosphenes3.png') }}" alt="">
        </div>
        <div class="col-lg-6">
          <div class="voffset50"></div>
          <div class="quote">
            <p>"If I can play one note and make you cry, then that's better than thouse fancy dancers playing twenty
              notes."</p>
            <p class="author">Robbie Robertson</p>
          </div>
          <div class="description">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam iure blanditiis velit, suscipit quidem
              fuga, magni repudiandae atque placeat sint corporis commodi praesentium dolore necessitatibus minima nemo
              ipsam, perspiciatis libero, quos. Obcaecati consectetur vel nostrum praesentiu.</p>
            <p>Obcaecati consectetur vel nostrum praesentium dolore necessitatibus minima nemo ipsam, perspiciatis
              libero, quos, odio quaerat asperiores repudiandae atque placeat sint corporis commodi onsectetur
              adipisicing elit.</p>
          </div>
        </div>
      </div>
      <div class="voffset150"></div>
    </div>
  </section>

  <!-- DISCOGRAPHY -->
  <section class="section discography inverse-color" id="discography">
    <div id="discography"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
          <div class="separator-icon">
            <i class="fa fa-music"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">Youtube hightlights</p>
          <div class="voffset20"></div>
          <h2 class="title">Discography</h2>
          <div class="voffset80"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ul class="carousel-discography js-flickity"
            data-flickity-options='{ "cellAlign": "left", "autoPlay":1500, "wrapAround": true, "contain": true, "prevNextButtons": false }'>
            <!-- col-xlg-3 -->
            @foreach($videos as $video)
            <li class="gallery-cell col-xs-12 col-sm-6 col-md-4">
              <div class="info-album">
                <div class="cover open-disc" data-url="discs/disc-01.html">
                  <img class="video-image" src="{{ $video['image']['image_url'] }}" alt="">
                  <a href="{{ $video->url }}" target="_blank">
                    <div class="rollover">
                      <i class="fa fa-youtube-play"></i>
                      <p>Play Song</p>
                    </div>
                  </a>
                </div>
                <p class="album">{{ $video->title }}</p>
                <p class="artist">{{ $video->subtitle }}</p>
              </div>
            </li>
            @endforeach
{{--             
            <li class="gallery-cell col-xs-12 col-sm-6 col-md-4">
              <div class="info-album">
                <div class="cover open-disc" data-url="discs/disc-02.html">
                  <img src="{{ asset('assets/frontend/images/demo/intro/youtube songs/dust.png') }}" alt="">
                  <a href="https://www.youtube.com/watch?v=ZDRO1hUPdyc" target="_blank">
                    <div class="rollover">
                      <i class="fa fa-youtube-play"></i>
                      <p>Play Song</p>
                    </div>
                  </a>
                </div>
                <p class="album">Dust</p>
                <p class="artist">Phosphenes</p>
              </div>
            </li> --}}
          </ul>
          <div class="voffset90"></div>
        </div>
      </div>
    </div>
    <!-- DETAILS DISCO -->
    <div id="project-show"></div>
    <div class="section player-album project-window">
      <div class="project-content"></div><!-- AJAX Dinamic Content -->
    </div>
  </section>

  <!-- TOUR DATES -->
  <!-- <section class="section full-width tourdates" id="anchor05">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="voffset70"></div>
            <div class="separator-icon">
              <i class="fa fa-microphone"></i>
            </div>
            <div class="voffset30"></div>
            <p class="pretitle">get ready for the live show</p>
            <div class="voffset20"></div>
            <h2 class="title">Gig Alearts</h2>
            <div class="voffset80"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="image-tour"></div>
          </div>
          <div class="col-sm-6">
            <div class="tour-info">
              <ul class="carousel-dates jcarouselDates">
                <li class="gallery-cell" id="tour1">
                  <div class="vcenter">
                    <p class="name-tour">Built<br>to spill</p>
                    <p class="separator">live</p>
                    <p class="subtitle-tour">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum atque tempore iste.</p>
                    <p class="buy">
                      <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                    </p>
                  </div>
                </li>
                <li class="gallery-cell" id="tour2">
                  <div class="vcenter">
                    <p class="name-tour">Show in Kathmandu Nepal</p>
                    <p class="separator">live</p>
                    <p class="subtitle-tour">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum atque tempore iste.</p>
                    <p class="buy">
                      <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                    </p>
                  </div>
                </li>
                <li class="gallery-cell" id="tour3">
                  <div class="vcenter">
                    <p class="name-tour">Live in Patan Museum</p>
                    <p class="separator">live</p>
                    <p class="subtitle-tour">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum atque tempore iste.</p>
                    <p class="buy">
                      <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                    </p>
                  </div>
                </li>
              </ul>
              <ul class="dates-tour button-group">
                <li class="button active">
                  February
                  <span>12</span>
                </li>
                <li class="button">
                  February
                  <span>14</span>
                </li>
                <li class="button">
                  February
                  <span>16</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section> -->

  <!-- UPCOMMING EVENTS -->
  <!-- <section class="section full-width upcomming-events-list inverse-color">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h4 class="upcomming-events-list-title">upcoming events</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li>
                <div>
                  <p class="date-event">21 <span>Jan'16</span></p>
                  <p class="name">
                    <span>Live show  New York</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Madison Square Garden</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>
              <li>
                <div>
                  <p class="date-event">25 <span>Jan'16</span></p>
                  <p class="name">
                    <span>gigs Set & performance Los Angeles</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Glam Club</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>
              <li>
                <div>
                  <p class="date-event">01 <span>Feb'16</span></p>
                  <p class="name">
                    <span>Live Show  Boston</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Howard Theater</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>

              <li class="more">
                <div>
                  <p class="date-event">15 <span>Feb'16</span></p>
                  <p class="name">
                    <span>gigs Set & performance Los Angeles</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Sugar Night Club</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>
              <li class="more">
                <div>
                  <p class="date-event">24 <span>Feb'16</span></p>
                  <p class="name">
                    <span>Live Show London</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Monarch Theater</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>
              <li class="more">
                <div>
                  <p class="date-event">05 <span>Mar'16</span></p>
                  <p class="name">
                    <span>Live Show  New York</span> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab enim perferendis, doloremque esse.
                  </p>
                  <p class="venue">
                    venue
                    <span>Madison Square Garden</span>
                  </p>
                  <p class="price">
                    tickets
                    <span>$19</span>
                  </p>
                  <p class="buy">
                    <a href="#" class="btn rounded icon"><i class="fa fa-ticket"></i> buy ticket</a>
                  </p>
                </div>
              </li>
            </ul>
            <p class="view-all-events">
              <a id="more-events" href="#" class="btn rounded border">View all events</a>
            </p>
          </div>
        </div>
      </div>
    </section> -->

  <!-- LATEST MEDIA -->
  <section class="section last-media inverse-color" id="gallery">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 gallery-title">
          <div class="voffset90"></div>
          <div class="separator-icon">
            <i class="fa fa-picture-o"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">Gallery</p>
          <div class="voffset20"></div>
          <h2 class="title">latest media</h2>
          <div class="voffset50"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="voffset50"></div>
          <div class="row">
            @foreach($instagram_post as $key=>$value)
                <!-- Image -->
                <div class="col-md-3 thumbnail">
                    <a href="{{ $value->node->display_url }}" class="swipebox" title="{{ $value->node->edge_media_to_caption->edges[0]->node->text }}" data-group="gallery-grid">

                      <!-- Image -->
                      <img src="{{ $value->node->thumbnail_src }}" alt="Gallery image" class="loaded">

                      <!-- Icon -->
                      <div class="rollover">
                        <i class="plus"></i>
                      </div>
                    </a>
                </div>
            @endforeach
          </div>
        </div>
      </div>

      <!-- gallery -->
      <div class="row">
        <div class="col-md-12">
          <div class="voffset50"></div>
          <div class="thumbnails">
            {{-- <div class="thumbnail video">
                <a href="https://vimeo.com/5182570" class="swipebox">
                  <img src="{{ asset('assets/frontend/images/demo/intro/gallery/8.jpg') }}" alt="">
                  <div class="rollover">
                    <i class="video"></i>
                  </div>
                </a>
              </div> --}}
            @foreach($galleries as $image)
            <div class="thumbnail">
                <a href="{{ $image['image']['image_url'] }}" class="swipebox">
                <img src="{{ $image['image']['image_url'] }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            @endforeach
            {{-- <div class="thumbnail music">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/2.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/2.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail gigs">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/3.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/3.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail small events">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/4.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/4.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail small indie">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/5.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/5.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail small music">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/6.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/6.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
          </div> --}}
          {{-- <div class="voffset50"></div>
          <p class="loadmore">
            <a id="append" href="#" class="btn rounded border">Load more</a>
          </p>
          <div id="more-items">
            <div class="thumbnail video">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/7.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/7.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail concert">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/8.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/8.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail small gigs">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/4.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/4.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
            <div class="thumbnail small events">
              <a href="{{ asset('assets/frontend/images/demo/intro/gallery/3.jpg') }}" class="swipebox">
                <img src="{{ asset('assets/frontend/images/demo/intro/gallery/3.jpg') }}" alt="">
                <div class="rollover">
                  <i class="plus"></i>
                </div>
              </a>
            </div>
          </div>
          <div class="voffset80"></div> --}}
        </div>
      </div>
    </div>
  </section>


  <!-- CONTACTS -->
  <section class="section inverse-color contact" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
          <div class="separator-icon">
            <i class="fa fa fa-microphone"></i>
          </div>
          <div class="voffset30"></div>
          <p class="pretitle">get in touch</p>
          <div class="voffset20"></div>
          <h2 class="title">say hello!</h2>
          <div class="voffset80"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-7">
          @include('layouts.frontend.flash-message')
          <form action="/message" method="post" id="contactform" class="contact-form">
            @csrf
            <div class="form-group">
              <label class="title small" for="name">Your name:</label>
              <input type="text" placeholder="Full Name" maxlength="100" name="name" id="name" class="text name required" required>
            </div>

            <div class="form-group">
              <label class="title small" for="email">Your email:</label>
              <input type="email" placeholder="Your Email" maxlength="150" name="email" id="email" class="text email required" required>
            </div>

            <div class="form-group">
              <label class="title small" for="message">Your message:</label>
              <textarea name="message" class="text area required" maxlength="500" id="message" placeholder="Type Message" required></textarea>
            </div>

            <!-- <div class="formSent"><p><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.</p></div> -->
            <input type="submit" value="Submit" class="btn rounded">
            <div class="voffset80"></div>
          </form>
        </div>
        <div class="col-sm-6 col-md-5">
          <div class="col-contact">
            <h4 class="title small">For Booking</h4>
            <div class="voffset20"></div>
            <p>Dhobighat, Lalitpur</p>
            <p>Alpas Technology Pvt. Ltd.</p>
            <ul class="contact">
              <li><i class="fa fa-phone"></i> 989898989898</li>
              <li><i class="fa fa-envelope"></i> booking@phosphenes.com</li>
            </ul>
            <h4 class="title small">Get socialized with us</h4>
            <ul class="social-links">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@push('scripts')
<script src="{{ asset('assets/frontend/scripts/jssor.slider-28.0.0.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    window.jssor_1_slider_init = function() {

        var jssor_1_SlideoTransitions = [
          [{b:-1,d:1,ls:0.5},{b:0,d:1000,y:5,e:{y:6}}],
          [{b:-1,d:1,ls:0.5},{b:200,d:1000,y:25,e:{y:6}}],
          [{b:-1,d:1,ls:0.5},{b:400,d:1000,y:45,e:{y:6}}],
          [{b:-1,d:1,ls:0.5},{b:600,d:1000,y:65,e:{y:6}}],
          [{b:-1,d:1,ls:0.5},{b:800,d:1000,y:85,e:{y:6}}],
          [{b:-1,d:1,ls:0.5},{b:500,d:1000,y:195,e:{y:6}}],
          [{b:0,d:2000,y:30,e:{y:3}}],
          [{b:-1,d:1,rY:-15,tZ:100},{b:0,d:1500,y:30,o:1,e:{y:3}}],
          [{b:-1,d:1,rY:-15,tZ:-100},{b:0,d:1500,y:100,o:0.8,e:{y:3}}],
          [{b:500,d:1500,o:1}],
          [{b:0,d:1000,y:380,e:{y:6}}],
          [{b:300,d:1000,x:80,e:{x:6}}],
          [{b:300,d:1000,x:330,e:{x:6}}],
          [{b:-1,d:1,r:-110,sX:5,sY:5},{b:0,d:2000,o:1,r:-20,sX:1,sY:1,e:{o:6,r:6,sX:6,sY:6}}],
          [{b:0,d:600,x:150,o:0.5,e:{x:6}}],
          [{b:0,d:600,x:1140,o:0.6,e:{x:6}}],
          [{b:-1,d:1,sX:5,sY:5},{b:600,d:600,o:1,sX:1,sY:1,e:{sX:3,sY:3}}]
        ];

        var jssor_1_options = {
          $AutoPlay: 1,
          $LazyLoading: 1,
          $CaptionSliderOptions: {
            $Class: $JssorCaptionSlideo$,
            $Transitions: jssor_1_SlideoTransitions
          },
          $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
          },
          $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$,
            $SpacingX: 20,
            $SpacingY: 20
          }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 1600;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    };
</script>
<script type="text/javascript">
  jssor_1_slider_init();
</script>
@endpush