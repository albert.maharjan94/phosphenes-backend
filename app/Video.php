<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $fillable=['title','subtitle','url','is_featured'];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
