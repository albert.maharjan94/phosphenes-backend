<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Gallery;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $galleries=Gallery::with(['image'])->orderby('is_featured','desc')->get();

        return view('backend.galleries.index',compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageHelper $file)
    {
        //
        $validate=[
            'description'=>'required|max:300',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ];

        $arr=$request->only(['description']);

        $img=$file->prepareStoreImage($request,'gallery');

        $g=Gallery::create($arr);
        $g->image()->save($img);
        return redirect('/admin/galleries')->with('success','Successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $gallery=Gallery::with(['image'])->findOrFail($id);
        return view('backend.galleries.edit',compact('gallery'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, ImageHelper $file)
    {
        //
        $gallery=Gallery::findOrFail($id);
        $validate=[
            'description'=>'required|max:300',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ];
        $arr=$request->only(['description']);

        $img=$file->prepareUpdateImage($request,$gallery,'gallery','gallery');

        $gallery->update($arr);
        $gallery->image()->update($img);
        return redirect('/admin/galleries')->with('success','Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ImageHelper $file)
    {
        //
        $gallery=Gallery::with(['image'])->findOrFail($id);
        
        
        if($gallery->type==0){
            $file->deleteImage('gallery',$gallery['image']['image_name']);
        }
        $gallery->image()->delete();
        $gallery->delete();

        return redirect('/admin/galleries')->with('success','Successfully deleted');
    }

    public function featured($id)
    {
        $gallery=Gallery::findOrFail($id);
        if($gallery->is_featured==1){
            $gallery->update(['is_featured'=>0]);
            return redirect('admin/galleries')->with('success','Successfully updated.');
        }else{
            $gallery->update(['is_featured'=>1]);
            return redirect('admin/galleries')->with('success','Successfully updated.');
        }
    }
    
}
