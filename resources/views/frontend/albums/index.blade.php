@extends('layouts.frontend.app') 
@section('albums','active')
@push('styles')
<style>
    .album{
        background:url("{{ asset('/assets/frontend/images/demo/intro/indiebg.png') }}") !important;
        background-size: 101% !important;
        background-position: center !important;
    }
    .text-center{
        width: 100%;
    }
</style>
@endpush
@section('content')
<section class="section full-width tourdates">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="voffset70"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12" style="padding:0">
        <div class="image-tour album" style="">
            <div class="vcenter text-center text-overlay">
                <h1 class="coming-title">Albums</h1>
                <div class="voffset30"></div>
                <div class="row">
                    <div class="col-md-12">
                      <ul class="carousel-discography">
                        <!-- col-xlg-3 -->
                        @foreach($albums as $album)
                        <li class="gallery-cell col-xs-12 col-sm-6 col-md-4">
                          <div class="info-album  inverse-color">
                            <div class="cover open-disc" data-url="discs/disc-01.html">
                              <img class="video-image" src="{{ $album['image']['image_url'] }}" alt="">
                                <a href="/albums/{{ $album->slug }}">
                                <div class="rollover">
                                  <i class="fa fa-play"></i>
                                  <p>View Album</p>
                                </div>
                              </a>
                            </div>
                            <p class="album">{{ $album->title }}</p>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                </div>
        
        
        
        
              </div>
        </div>
        </div>
        
    </div>
    </div>
  </section>
@endsection