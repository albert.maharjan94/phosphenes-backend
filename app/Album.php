<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    //
    protected $fillable=['title','description','compressed_url','compressed_name','slug'];

    public function image(){
        return $this->morphOne('App\Image', 'imageable');
    }

    public function songs(){
        return $this->hasMany(Song::class);
    }

    public function codes(){
        return $this->hasMany(Code::class);
    }
}
