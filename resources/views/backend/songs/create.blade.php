@extends('layouts.admin.app')
@section('title','Song')
@section('songs','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/songs">Song</a></li>
    <li class="active">Create</li>
@endsection
@section('content')
<style>
    .mt-2{
        margin-top: 5px;
    }
</style>
<section class="content">
    @include('layouts.admin.flash-message')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-6">
                    <h3 class="box-title">Add Song</h3>
                    </div>
                </div>
            </div>
        <form role="form" id="p-form" action="/admin/songs" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
            <div class="form-group">
                <label for="album_id">Album Title</label>
                <select name="album_id" class="select form-control" title="Select an album">
                    @foreach($albums as $album)
                    <option value="{{ $album->id }}">{{ $album->title }}</option>
                    @endforeach
                </select>
                {{-- <input type="text" class="form-control text-area" name="title" placeholder="Enter Title" required> --}}
            </div>
            <div class="form-group">
                <label for="title">Song Title</label>
                <input type="text" class="form-control text-area" name="title" value="{{ old('title') }}" placeholder="Enter Title" required>
            </div>
            <div class="form-group">
                <label for="lyrics">Song Lyrics</label>
                <textarea class="form-control text-area" style="max-width:100%;min-width:100%;max-height:300px;" name="lyrics" placeholder="Enter Lyrics" required>{{ old('lyrics') }}</textarea>
            </div>
            <div class="form-group">
                <label for="duration">Song Duration</label>
                <input type="text" class="form-control text-area" name="duration" value="{{ old('duration') }}" placeholder="Enter Duration" required>
            </div>            
            <div class="form-group div-image">
                <label for="song">Sound File</label>
                <input type="file" id="song" required accept="audio/*" name="song" >

                <p class="help-block">Size is limited to 8 MB</p>
            </div>
           
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            
            <a href="/admin/songs/" class="btn btn-warning">Return</a>
            </div>
        </form>
        </div>
        </div>
    </div>
</section>
@endsection
@push('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap3/bootstrap-switch.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('assets/admin/js/bootstrap-switch.min.js') }}"></script>
<script>
    

        $('#song').on('change',function(e){
            var file=this.files[0]
            var elem=$(this);
			if(file.size/(1024*1024)>10){
                toastr.error('Audio size is limited to 10MB only.');
                elem.val('');
            }
        });

    })

    $('#p-form').on('submit',function(e){
        if($('input[name="title"]').val().length>120)
        {
            toastr.error('Title is limited to 120 words only');
            $('input[name="title"]').focus();
            e.preventDefault();
            $.unblockUI();
            return false;
        }
        if($('input[name="lyrics').length>1000){
            toastr.error('Lyrics is limited to 1000 words only');
            $('input[name="lyrics').focus();
            e.preventDefault();
            $.unblockUI();
            return false;
        }
        if($('input[name="duration').length>1000){
            toastr.error('Duration is limited to 10 words only');
            $('input[name="duration').focus();
            e.preventDefault();
            $.unblockUI();
            return false;
        }
        
    });
    // $('.div-image')


</script>
@endpush