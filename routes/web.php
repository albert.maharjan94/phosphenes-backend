<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/getCode',function(){
    $arr=[];
    $count=1;
    array_push($arr,\Str::Random(6));
    while($count<500){
        $rand=\Str::Random(6);
        if(!in_array($arr,[$rand])){
            array_push($arr,\Str::Random(6));
            $count+=1;
        }
        
    }
    foreach($arr as $a){
        \App\Code::create(['album_id'=>1,'token'=>$a]);
    }
});

Route::get('/','HomeController@index');
Route::get('/events/{slug}','HomeController@showEvents');
Route::post('/message','HomeController@message');

Route::group(['middleware'=>'is.user'],function(){
    Route::resource('albums', 'AlbumController')->only(['index','show']);
    Route::post('/download-album','AlbumController@download');
    Route::get('/albums/download/{id}','AlbumController@downloadLink');
});


// route for user register
Route::group(['middleware'=>'guest'], function () {
    Route::get('/login','AuthController@showLogin');
    Route::post('/login','Auth\LoginController@login');
    Route::get('/register','AuthController@showRegister');
    Route::post('/register','AuthController@register');
    
    Route::get('/forget','Auth\Frontend\ForgotPasswordController@showLinkRequestForm');
    Route::post('/reset-password','Auth\Frontend\ForgotPasswordController@sendResetLinkEmail');
    
    Route::get('/password-reset/{token}','Auth\Frontend\ResetPasswordController@showResetForm');
    Route::post('/password-reset','Auth\Frontend\ResetPasswordController@reset');
    
});
// social auth
/* Social Login Routes Start */
Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' );

Route::get ( '/callback/{service}', 'SocialAuthController@callback' );
/* Social Login Routes End */

Route::group(['prefix' => 'admin'],function(){
    Auth::routes();
});

Route::group(['prefix' => 'admin','middleware'=>'is.admin'], function () {
    
    Route::resource('dashboard', 'Admin\DashboardController');

    Route::resource('galleries', 'Admin\GalleryController');
    Route::patch('galleries/featured/{id}','Admin\GalleryController@featured');

    Route::resource('albums', 'Admin\AlbumController');
    Route::get('/albums/get-codes/{id}','Admin\AlbumController@getCodes');
    Route::get('albums/compressed/{id}','Admin\AlbumController@createCompressed');
    Route::patch('albums/compressed/{id}','Admin\AlbumController@storeCompressed');
    
    Route::resource('events', 'Admin\EventController');
    
    Route::resource('songs', 'Admin\SongController');

    Route::resource('videos', 'Admin\VideoController');
    Route::patch('videos/featured/{id}','Admin\VideoController@featured');

    Route::resource('users', 'Admin\UserController');

    Route::get('/messages','Admin\MessageController@index');
    Route::delete('/messages/{id}','Admin\MessageController@destroy');
    
});

Route::get('/home', 'HomeController@index')->name('home');
