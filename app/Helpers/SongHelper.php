<?php


namespace App\Helpers;

use Illuminate\Support\Facades\File;
use App\Song;

class SongHelper
{

    public function storeSong($request,$type)
    {
        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/songs/'), $fileName);
        $song=['audio_url'=>asset('storage/songs/'.$fileName),'audio_name'=>$fileName];
        return $song;
    }

    public function deleteSong($filename)
    {
        if ( File::exists(public_path('storage/songs/'). $filename))
        {
            File::delete(public_path('storage/songs/') . $filename);
        }

        return;
    }

    public function updateSong($request, $oldFile = null,$type)
    {
        $this->deleteSong($oldFile);

        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/songs/'), $fileName);
        
        $song=['audio_url'=>asset('storage/songs/'.$fileName),'audio_name'=>$fileName];
        return $song;
    }
}