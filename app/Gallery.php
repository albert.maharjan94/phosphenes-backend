<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    protected $fillable=['description','url','type','image_name','is_featured'];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
