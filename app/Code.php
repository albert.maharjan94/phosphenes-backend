<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    //
    protected $fillable=['token','user_id','album_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function album(){
        return $this->belongsTo(Album::class);
    }
}
