<?php

namespace App\Http\Controllers\Admin;

use App\Song;
use App\Album;
use App\Helpers\SongHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SongController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $songs=Song::with(['album','album.image'])->get();
        return view('backend.songs.index',compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $albums=Album::get();
        return view('backend.songs.create',compact('albums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,SongHelper $file)
    {
        //
        // dd($request->all());
        request()->validate([
            'title'=>'required|max:120',
            'lyrics'=>'required|max:1000',
            'song'=>'required|max:812400',
            'album_id'=>'required',
            'duration'=>'required',
        ]);
        // dd('here');
        $album=Album::whereId($request->album_id)->first();
        if(!$album){
            return back()->with('error','Album not found');
        }

        $arr=$request->only(['title','lyrics','album_id','duration']);

        $arr=array_merge($arr,$file->storeSong($request->song,'song'));

        Song::create($arr);

        return redirect('admin/songs')->with('success','Successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $song=Song::with(['album'])->findOrFail($id);
        
        $albums=Album::get();
        return view('backend.songs.edit',compact('song','albums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,SongHelper $file)
    {
        //
        $song=Song::with(['album'])->findOrFail($id);
        request()->validate([
            'title'=>'required|max:120',
            'lyrics'=>'required|max:1000',
            'audio'=>'max:812400',
            'album_id'=>'required',
            'duration'=>'required',
        ]);

        $album=Album::whereId($request->album_id)->first();

        if(!$album){
            return back()->with('error','Album not found');
        }
        $arr=$request->only(['title','lyrics','album_id','duration']);
        if($request->audio){
            $arr=array_merge($arr,$file->updateSong($request->audio,$song->audio_name,'song'));
        }

        $song->update($arr);

        
        return redirect('admin/songs')->with('success','Successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,SongHelper $file)
    {
        //
        $song=Song::with(['album'])->findOrFail($id);
        $file->deleteSong($song->audio_name);
        $song->delete();
        return redirect('admin/songs')->with('success','Successfully deleted');
    }
}
