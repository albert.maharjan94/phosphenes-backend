<?php


namespace App\Helpers;

use Illuminate\Support\Facades\File;
use App\Album;

class CompressedHelper
{

    public function storeZip($request,$type)
    {
        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/zip/'), $fileName);
        $song=['compressed_url'=>asset('storage/zip/'.$fileName),'compressed_name'=>$fileName];
        return $song;
    }

    public function deleteZip($filename)
    {
        if ( File::exists(public_path('storage/zip/'). $filename))
        {
            File::delete(public_path('storage/zip/') . $filename);
        }

        return;
    }

    public function updateZip($request, $oldFile = null,$type)
    {
        $this->deleteZip($oldFile);

        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/zip/'), $fileName);
        
        $song=['compressed_url'=>asset('storage/zip/'.$fileName),'compressed_name'=>$fileName];
        return $song;
    }
}