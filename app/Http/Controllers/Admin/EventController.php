<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Helpers\SlugHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events=Event::with(['image'])->get();
        return view('backend.events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view('backend.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ImageHelper $file,SlugHelper $slug)
    {
        //
        // dd($request->all());
        request()->validate([
            'title'=>'required|max:120',
            'description'=>'required|max:300',
            'date'=>'required',
            'time'=>'required',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ]);
        $s=$slug->slugify($request->title);
        if(Event::where('slug',$s)->count()){
            return back()->with('error','Event title already exist.');
        };
        $arr=array_merge($request->only(['description','title']),
        [
            'slug'=>$s,
            'date'=>date('yy-m-d',strtotime($request->date)),
            'time'=>date('H:i:s',strtotime($request->time))
        ]);
          
        $img=$file->prepareStoreImage($request,'event');

        $event=Event::create($arr);
        $event->image()->save($img);
        return redirect('/admin/events')->with('success','Successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event=Event::findOrFail($id);
        return view('backend.events.edit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ImageHelper $file,SlugHelper $slug)
    {
        //
        $event=Event::findOrFail($id);
        request()->validate([
            'title'=>'required|max:120',
            'description'=>'required|max:300',
            'date'=>'required',
            'time'=>'required',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ]);

        $s=$slug->slugify($request->title);

        if(Event::where('id','<>',$event->id)->where('slug',$s)->count()){
            return back()->with('error','Event title already exist.');
        };
        $arr=array_merge($request->only(['description','title']),
        [
            'slug'=>$s,
            'date'=>date('yy-m-d',strtotime($request->date)),
            'time'=>date('H:i:s',strtotime($request->time))
        ]);

        
        $img=$file->prepareUpdateImage($request,$event,'event','event');

        $event->update($arr);
        
        $event->image()->update($img);
        
        return redirect('admin/events')->with('success','Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ImageHelper $file)
    {
        //
        $event=Event::with(['image'])->findOrFail($id);
        
        if($event->type==0){
            $file->deleteImage('event',$event['image']['image_name']);
        }
        $event->image()->delete();
        $event->delete();

        return redirect('/admin/events')->with('success','Successfully deleted');
    }
}
