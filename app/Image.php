<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable=['image_url','image_name','type','is_featured'];

    public function imageable()
    {
        return $this->morphTo();
    }
}
