<?php

namespace App\Http\Controllers;

use App\Event;
use App\Video;
use App\Gallery;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events=Event::with(['image'])->where('date','>',now())->orderby('date','desc')->limit(3)->get()->map(function($event){
            $date=Carbon::parse($event->date)->diff();
            // dump(Carbon::parse($event->date)->diffInHours());
            return [
                'id'=>$event->id,
                'created_at'=>date('jS \of F Y',strtotime($event->created_at)),
                'date'=>$date,
                'image'=>$event['image']['image_url'],
                'title'=>$event['title'],
                'description'=>$event['description'],
                'slug'=>$event['slug']
            ];
        });
        $galleries=Gallery::with(['image'])->orderby('created_at','desc')->whereIsFeatured(0)->limit(8)->get();
        $banners=Gallery::with(['image'])->orderby('created_at','desc')->whereIsFeatured(1)->get();


        $client = new \GuzzleHttp\Client();

        $response = json_decode($client->get("https://www.instagram.com/phosphenes_the/?__a=1")->getbody());

        $instagram_post= !empty($response) ? array_slice($response->graphql->user->edge_owner_to_timeline_media->edges,0,8) : [];
        
        $videos=Video::with(['image'])->whereIsFeatured(1)->orderBy('created_at','desc')->limit(8)->get();
        return view('frontend.index',compact('events','galleries','videos','banners','instagram_post'));
    }

    public function showEvents($slug)
    {
        $event=Event::with(['image'])->whereSlug($slug)->first();
        if(!$event){
            return back();
        }

        return view('frontend.events.show',compact('event'));
    }

    public function message(Request $request)
    {
        request()->validate([
            'name'=>'required|max:100',
            'email'=>'required|max:150',
            'message'=>'required|max:500'
        ]);
        $chk=Session::get('hasMessage');
        // dump($chk);

        if($chk){
            return redirect('/')->with('error','To send message again wait 120 minutes.');
        }
        Message::create($request->only(['name','email','message']));
        Session::put('hasMessage',true);
        
        return redirect('/')->with('success','Your message has been successfully sent.');
    }
}
