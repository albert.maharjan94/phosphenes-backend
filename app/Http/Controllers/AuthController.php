<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Mail\UserPasswordReset;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    public function showLogin()
    {
        return view('frontend.auth.login');
    }


    public function showRegister()
    {
        return view('frontend.auth.register');
    }

    public function showForget()
    {
        return view('frontend.auth.forget');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    //Create normal user
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        $userSocial = User::create([
            'name'          => $request['name'],
            'role'          => 1,
            'email'         => $request['email'],
            'password'      => Hash::make($request['password']),
        ]);

        Auth::login($userSocial);   
        return redirect('/albums');
    }

    public function passwordReset(Request $request)
    {
        // dd($request->all());
        
        request()->validate([
            'email'=>'required|exists:users,email'
        ],[
            'email.exists'=>'The email is not registered.',
        ]);
        $user=User::whereEmail($request->email)->first();

        Mail::to($request->email)->send(new UserPasswordReset($user));
        return redirect('/login')->with('success','Password reset email sent.');
    }

    public function passwordChange($token = null)
    {
        $id=Crypt::decryptString($token);
        $user=User::findOrFail($id);
        return view('frontend.auth.reset')->with(
            ['user'=>$user,'token'=>$token]
        );;
    }

    public function passwordUpdate(Request $request,$user)
    {
        // dd($user);
        request([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user=User::findOrFail(Crypt::decryptString($user));
        // dd(Crypt::decryptString($user));
        // dd($user);
        $user->update(['password'=>bcrypt($request->password)]);
        return redirect('/login')->with(['success'=>'Password reset successfully']);
    }


   
}
