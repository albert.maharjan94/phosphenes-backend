@extends('layouts.frontend.auth')
@section('content')
<div class="limiter">
    <div class="container-login100" style="background-image: url('/assets/frontend/auth/images/bg-01.jpg');">
    <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
    <form action="/password-reset" method="POST" class="login100-form validate-form flex-sb flex-w">
            @csrf
            
            <input type="hidden" name="token" value="{{ $token }}">
            @include('layouts.frontend.flash-message')
            <span class="login100-form-title p-b-53">
                Reset Password
            </span>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Email
                </span>
            </div>
            <div class="wrap-input100 validate-input">
            <input class="input100" type="email" value="{{ $email }}" readonly name="email" required>
                <span class="focus-input100"></span>
            </div>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Password
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <input class="input100" type="password" name="password" required>
                <span class="focus-input100"></span>
            </div>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Confirm Password
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Confirm password is required">
                <input class="input100" type="password" name="password_confirmation" required>
                <span class="focus-input100"></span>
            </div>
            <div class="w-100 my-2">
                <a href="/login" class="txt2 bo1 m-l-5">
                    Back to login?
                </a>
                </div>
            <div class="container-login100-form-btn m-t-17">
                <button class="login100-form-btn">
                    Reset
                </button>
                
            </div>


        </form>
    </div>
</div>
</div>

@endsection