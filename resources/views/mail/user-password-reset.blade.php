@component('mail::message')
Dear {{ $name }},

You've asked for your password to reset. Please click on the link below to proceed.

@component('mail::button', ['url' => url('/password-reset/'.$token.'/?email='.$email)])
Reset Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
