@extends('layouts.admin.app')
@section('title','Albums')
@section('albums','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Albums</li>
@endsection
@section('content')

<div class="modal fade" id="modal-delete" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Delete?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this album?</p>
        </div>
        <div class="modal-footer">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Continue</button>
            </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@include('layouts.admin.flash-message')
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
              <div class="row">
                  <div class="col-lg-6">
                    <h3 class="box-title">List of Albums</h3>
                  </div>
                  <div class="col-lg-6 text-right d-flex">
                      <a href="/admin/albums/create" class="btn btn-primary"><i class="fa fa-plus">  Add</i></a>
                      
                      
                  </div>
              </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Id</th>
                <th>Image</th>
                <th>Title</th>
                <th>Codes</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @foreach($albums as $key=>$album)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td><img class="table-image image image-fluid" src="{{ $album['image']['image_url'] }}"></td>
                    <td>{{ $album->title }}</td>
                  <td>@if($album['codes'])<a target="_blank" class="btn btn-default" href="/admin/albums/get-codes/{{ $album->id }}"> Get Codes </a>@endif</td>
                    <td>{{ $album->description }}</td>
                    <td>
                        <a class="btn btn-sm btn-icon btn-warning" href="/admin/albums/{{ $album->id }}/edit"><i class="fa fa-edit"></i> Edit</a>
                        <a class="btn btn-sm btn-icon btn-danger del" data-id="{{ $album->id }}"><i class="fa fa-trash"></i> Delete</a>
                        <a href="/admin/albums/compressed/{{ $album->id }}" class="btn btn-sm btn-icon bg-navy"><i class="fa fa-file-zip-o "></i> Zip</a>
                    </td>
                </tr>
              @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Id</th>
                <th>Image</th>
                <th>Title</th>
                <th>Codes</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection
@push('styles')
<style>
    .table-image{
       height: 40px;
       width: 40px;
       object-fit:cover; 
    }
</style>

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#example1').DataTable();

    $(document).on('click','.del',function(e){
        e.preventDefault();
        $('#modal-delete form').attr('action','/admin/albums/'+$(this).data('id'));
        $('#modal-delete').modal('show');
    })
</script>
@endpush