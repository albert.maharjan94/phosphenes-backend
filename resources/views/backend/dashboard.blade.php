@extends('layouts.admin.app')
@section('title','Dashboard')
@section('dashboard','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
            <h3>{{ $count['albums'] }}</h3>

            <p>Albums</p>
            </div>
            <div class="icon">
            <i class="fa fa-folder-open-o"></i>
            </div>
            <a href="/admin/albums" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
            <h3>{{ $count['messages'] }}</h3>

            <p>Messages</p>
            </div>
            <div class="icon">
            <i class="fa fa-envelope"></i>
            </div>
            <a href="/admin/messages" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
            <h3>{{ $count['users'] }}</h3>

            <p>Users</p>
            </div>
            <div class="icon">
            <i class="ion ion-person-add"></i>
            </div>
            <a href="/admin/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
            <h3>{{ $count['videos'] }}</h3>

            <p>Videos</p>
            </div>
            <div class="icon">
            <i class="fa fa-video-camera"></i>
            </div>
            <a href="/admin/videos" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
   
    <!-- /.row (main row) -->
</section>
@endsection