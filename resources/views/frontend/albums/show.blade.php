@extends('layouts.frontend.app')
@section('albums','active')
@section('content')
<section class="intro full-width full-height jIntro comingsoon" id="anchor00">
  <div class="modal modal-code" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Download</p>
        </div>
        <div class="modal-footer">
          {{-- <button type="button" class="btn rounded">Save changes</button> --}}
          <button type="button" class="btn rounded btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding:0">
          <div class="slider-intro">
            <div id="slides">
              {{-- <div class="slides-container"> --}}
                <img src="{{ $album['image']['image_url'] }}" style="opacity:0.5;width:100%">
              {{-- </div> --}}
            </div>
          </div>
        </div>
      </div>

      <div class="vcenter text-center text-overlay">
        <h1 class="coming-title">@if(auth()->user()) Welcome {{ auth()->user()->name }} @else Login to download  @endif</h1>
        <div class="voffset30"></div>

       




        <!-- time circles -->
        <!-- <div id="DateCountdown" data-date="2016-03-01 00:00:00"></div> -->
        @if($hasCode)
        <!-- emaill address -->
          @if($hasToken)
            <button onClick="location.href='/albums/download/{{ $album->id }}'" type="button" class="btn rounded download">Download</button>
          @else
          <h1 class="pretitle" style="color:white;">Enter the code, and download our album</h1>
          <div class="voffset30"></div>

          <form action="" method="post" id="newsletterform" class="newsletter-form code-input">
            <div class="form-group">
              <input type="text"  minlength="6" maxlength="6" placeholder="ENTER CODE." name="token" id="" class="text  required">
            </div>
            <input type="submit" value="Download" class="btn rounded download">
          </form>

          @endif
        @else
        <h1 class="pretitle" style="color:white;">No link for download available at the moment.</h1>
        <div class="voffset30"></div>
        @endif

        <div class="voffset60"></div>
        @if(!$album['songs']->isEmpty())
        <h3 class="text-preplayer">Quick view our songs</h3>
        <!-- PLAYER -->
        <div class="player horizontal">
          <div class="container">
            <div class="info-album-player">
                <div class="album-cover" style="background:url('{{ $album['image']['image_url'] }}');background-size:cover;" id="bg-image3"></div>
              <p class="album-title">Phosphenes</p>
                <p class="artist-name">{{ $album->title }}</p>
            </div>
            <div class="player-content">
              <audio preload></audio>
              <ol class="playlist">
                @foreach($album['songs'] as $song)
                    <li><a href="#" data-src="{{ $song['audio_url'] }}">{{ $song['title'] }}</a></li>
                @endforeach
              </ol>
              <div class="nextprev">
                <span class="prev">prev</span>
                <span class="next">next</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      @else
      <h3 class="text-preplayer">No songs available to play</h3>
      @endif

    </div>
  </section>

@endsection

@push('scripts')
<script>
  $('.code-input').on('submit',function(e){
    var token=$('input[name="token"]').val();
    e.preventDefault();
    $.ajax({
      'url':'/download-album',
      'data':{
          '_token': '{{ csrf_token() }}',
          'album_id':'{{ $album->id }}',
          'token':token,
      },
      'type':'POST',
      'dataType':'JSON',
      success:function(data){
        if(data.success){
          $('.modal-code').modal('show');
          $('.modal-code .modal-body p').html(`Your download will start soon. <br><a href="/albums/download/${data.url.id}">Click here.</a> if download have not started automatically.`);
          document.location.href = data.url.compressed_url;
        }else{
          $('.modal-code').modal('show');
          $('.modal-code .modal-body p').html(`Invalid code.`);
          
        }
         
      }
    });
  })
</script>
@endpush