@extends('layouts.frontend.app') 
@section('events','active')
@push('styles')
  <meta name="twitter:card" content="summary">
	<meta property="og:url" content="{{ env('APP_URL') }}/events/{{ $event->slug }}">
	<meta property="og:type" content="article">
	<meta property="og:title" content="Phosphenes - {{ $event->title }}">
	<meta property="og:description" content="{{ $event->description }}">
  <meta property="og:image" content="{{ $event['image']['image_url'] }}">
  <style>
    .button-pop{
      height: auto !important;
      padding:20px 0px !important;
    }
    .image-tour{
      background:url('{{ $event['image']['image_url'] }}') !important;
      background-size: cover !important; 
      background-position:center !important;
    }
  </style>
@endpush
@section('content')
 <!-- TOUR DATES -->
    <section class="section full-width tourdates">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="voffset70"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6" style="padding:0">
            <div class="image-tour"></div>
            </div>
            <div class="col-sm-6" style="padding:0">
              <div class="tour-info" >
                <ul class="carousel-dates jcarouselDates">
                  <li class="gallery-cell" id="tour1">
                    <div class="vcenter">
                        <p class="name-tour">{{ $event->title }}</p>
                        <p class="separator">live</p>
                        <p class="subtitle-tour">{{ $event->description }}</p>
                      <p class="buy">
                        <a href="https://www.facebook.com/sharer/sharer?m={{ $event['image']['image_url'] }}&t={{ $event->title  }}&u={{ env('APP_URL') }}/events/{{ $event->slug }}" target="_blank" class="btn rounded icon"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?text={{ urlencode($event->title) }}&url={{ env('APP_URL').'/events/'.$event->slug }}"  class="btn rounded icon"><i class="fa fa-twitter"></i></a>
                        {{-- <a href="#" class="btn rounded icon"><i class="fab fa-facebook-messenger"></i></a>   --}}
                        {{-- <a  href="https://www.facebook.com/dialog/send?link={{ env('APP_URL') }}/events/{{ $event->title }}&app_id={{ env('FACEBOOK_ID') }}&redirect_uri={{ env('APP_URL') }}" target="_blank" class="share-button"><span class='fab fa-facebook-messenger'></span></a> --}}
                      </p>
                    </div>
                  </li>
                </ul>
                <ul class="dates-tour button-group">
                  <li class="button active button-pop">
                    {{ date('Y-m',strtotime($event->date)) }}
                    <span>{{ date('d',strtotime($event->date)) }}</span>
                    {{ date('h:i A',strtotime($event->time)) }}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>

@endsection