<?php

namespace App\Http\Controllers;

use App\Code;
use App\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $albums=Album::with(['image'])->get();
        return view('frontend.albums.index',compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $album=Album::with(['image','songs'])->whereSlug($slug)->first();
        if(!$album){
            abort(404);
        }
        
        $hasCode=Code::whereAlbumId($album->id)->count()>0;
        $hasToken=Code::whereAlbumId($album->id)->whereUserId(auth()->user()->id)->count()>0;
        return view('frontend.albums.show',compact('album','hasToken','hasCode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download(Request $request)
    {
        $album=Album::findOrFail($request->album_id);
        $codes=Code::whereToken($request->token)->whereAlbumId($request->album_id)->first();
        if($codes){
            if($codes->user_id==null){
                $codes->update(['user_id'=>auth()->user()->id]);
            }else{
                if($codes->user_id!=auth()->user()->id){
                    return response()->json(['sucess'=>false]);
                }
            }
            return response()->json(['success'=>true,'url'=>$album]);
        }
        return response()->json(['sucess'=>false]);
    }

    public function downloadLink($id)
    {
        $codes=Code::whereUserId(auth()->user()->id)->whereAlbumId($id)->get();
        $album=Album::findOrFail($id);
        if(!$codes->isEmpty())
        {
            return redirect('/storage/zip/'.$album->compressed_name);
        }else{
            abort(403);
        }
    }
}
