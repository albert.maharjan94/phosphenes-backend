<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    //
    /**
	* Redirect the user to the social authentication page.
	*
	* @return \Illuminate\Http\Response
	*/
	public function redirect($service)
	{
	    return Socialite::driver($service)->redirect();
	}

    public function callback($service, Request $request) {
        // dd($service);
        if (!$request->has('code') || $request->has('denied')) {
            return redirect('/');
        }
        if ($service == 'google') {
            $userSocial = Socialite::driver( $service )->stateless()->user();
        }
        else {
            $userSocial = Socialite::with( $service )->user();
        }

        $users = User::where(['email' => $userSocial->getEmail()])->first();

        if($users){
            Auth::login($users);
        }
        else{
            $userSocial = User::create([
                'name'          => $userSocial->getName(),
                'role'       => 1,
                'email'         => $userSocial->getEmail(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $service,
            ]);

            Auth::login($userSocial);
        }

        return redirect('/albums');
    }

}
