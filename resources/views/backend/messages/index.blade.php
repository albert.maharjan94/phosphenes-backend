@extends('layouts.admin.app')
@section('title','Message')
@section('messages','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Messages</li>
@endsection
@section('content')

<div class="modal fade" id="modal-delete" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Delete?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this message?</p>
        </div>
        <div class="modal-footer">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Continue</button>
            </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--  --}}
<div class="modal fade" id="modal-dynamic" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>         
            </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--  --}}
@include('layouts.admin.flash-message')
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
              <div class="row">
                  <div class="col-lg-6">
                    <h3 class="box-title">List of Messages</h3>
                  </div>
              </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @foreach($messages as $key=>$message)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $message->name }}</td>
                    <td>{{ $message->email }}</td>
                    <td>{{ $message->message }}</td>

                    <td>
                        <a class="btn btn-sm btn-icon btn-danger del" data-id="{{ $message->id }}"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                </tr>
              @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection
@push('styles')

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#example1').DataTable();
    $(document).on('click','.del',function(e){
        e.preventDefault();
        $('#modal-delete form').attr('action','/admin/messages/'+$(this).data('id'));
        $('#modal-delete').modal('show');
    })
</script>
@endpush