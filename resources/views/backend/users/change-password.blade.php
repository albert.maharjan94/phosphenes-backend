@extends('layouts.admin.app')
@section('title','User')
@section('users','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/users">User</a></li>
    <li class="active">Create</li>
@endsection
@section('content')
<style>
    .mt-2{
        margin-top: 5px;
    }
</style>
<section class="content">
    @include('layouts.admin.flash-message')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-6">
                    <h3 class="box-title">Update password for {{ $user->name }}</h3>
                    </div>
                </div>
            </div>
        <form role="form" id="p-form" action="/admin/users/{{ $user->id }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="box-body">
            <div class="form-group">
                <label for="title">Email</label>
            <input type="text" disabled readonly class="form-control text-area" name="title" value="{{ $user->email }}" >
            </div>
            <div class="form-group">
                <label for="title">Password</label>
                <input type="password" class="form-control text-area" name="password" placeholder="Enter password" required>
            </div>
           
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            
            <a href="/admin/users/" class="btn btn-warning">Return</a>
            </div>
        </form>
        </div>
        </div>
    </div>
</section>
@endsection