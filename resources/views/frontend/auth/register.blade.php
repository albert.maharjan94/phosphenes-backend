@extends('layouts.frontend.auth')
@section('content')
<div class="limiter">
    <div class="container-login100" style="background-image: url('/assets/frontend/auth/images/bg-01.jpg');">
    <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
        <form id="register" class="login100-form validate-form flex-sb flex-w" action="/register" method="POST">
            @csrf
            
            @include('layouts.frontend.flash-message')
            <span class="login100-form-title p-b-53">
                Register
            </span>
            <div class="p-t-31 p-b-9">
                <span class="txt1">
                    Name
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Name is required">
                <input class="input100" type="text" name="name">
                <span class="focus-input100"></span>
            </div>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Email
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Email is required">
                <input class="input100" type="email" name="email">
                <span class="focus-input100"></span>
            </div>

            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Password
                </span>
            </div>
            
            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <input class="input100" type="password" name="password">
                <span class="focus-input100"></span>
            </div>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Confirm Password
                </span>
            </div>
            
            <div class="wrap-input100 validate-input">
                <input class="input100" type="password" name="password_confirmation" data-validate="Confirm password is required">
                <span class="focus-input100"></span>
            </div>

            <div class="container-login100-form-btn m-t-17">
                <button class="mt-2 login100-form-btn">
                    Register
                </button>
            </div>
            <div class="w-100 my-2">
            <a href="/login" class="txt2 bo1 m-l-5">
                Already have a login?
            </a>
            </div>
            <a href="/redirect/facebook" class="btn-face m-b-20">
                <i class="fa fa-facebook-official"></i>
                Facebook
            </a>

            <a href="/redirect/google" class="btn-google m-b-20">
                <img src="{{ asset('assets/frontend/auth/images/icons/icon-google.png') }}" alt="GOOGLE">
                Google
            </a>

            


        </form>
    </div>
</div>
</div>



@endsection
@push('styles')
<style>
    ul{
        text-align: left;
        font-size: 1rem;
    }
    .alert-danger{
        width: 100%;
    }
</style>
@endpush