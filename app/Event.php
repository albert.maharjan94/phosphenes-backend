<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $fillable=[
        'title','description','date','time','slug'
    ];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

}
