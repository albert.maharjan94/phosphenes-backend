<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\Helpers\SlugHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Helpers\CompressedHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class AlbumController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $albums=Album::with(['image','codes'])->get();
        // dd($albums);
        return view('backend.albums.index',compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ImageHelper $file,SlugHelper $slug)
    {
        //
        request()->validate([
            'title'=>'required|max:120',
            'description'=>'required|max:300',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ]);
        $s=$slug->slugify($request->title);
        if(Album::where('slug',$s)->count()){
            return back()->with('error','Album title already exist.');
        };
        $arr=array_merge($request->only(['description','title']),['slug'=>$s]);

        $img=$file->prepareStoreImage($request,'album');

        $album=Album::create($arr);
        $album->image()->save($img);
        return redirect('/admin/albums')->with('success','Successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $album=Album::with(['image'])->findOrFail($id);
        return view('backend.albums.edit',compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ImageHelper $file,SlugHelper $slug)
    {
        //
        $album=Album::findOrFail($id);
        request()->validate([
            'title'=>'required|max:120',
            'description'=>'required|max:300',
            'type'=>[
                Rule::in([0, 1]),
            ],
            'image'=>'max:512400',
        ]);
        $s=$slug->slugify($request->title);
        if(Album::where('id','<>',$album->id)->where('slug',$s)->count()){
            return back()->with('error','Album title already exist.');
        };
        $arr=array_merge($request->only(['description','title']),['slug'=>$s]);

        $img=$file->prepareUpdateImage($request,$album,'album','album');

        $album->update($arr);
        $album->image()->update($img);
        return redirect('/admin/albums')->with('success','Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ImageHelper $file,CompressedHelper $compressed)
    {
        //
        $album=Album::with(['image'])->findOrFail($id);
        
        
        if($album->type==0){
            $file->deleteImage('album',$album['image']['image_name']);
        }
        $album->image()->delete();
        $album->songs()->delete();
        $compressed->deleteZip($album->compressed_name);
        $album->delete();

        return redirect('/admin/albums')->with('success','Successfully deleted');
    }

    public function createCompressed($id)
    {
        $album=Album::findOrFail($id);
        // $file->updateZip($request->zip);
        // dd($album);
        return view('backend.albums.compressed',compact('album'));
    }

    public function storeCompressed(Request $request,$id,CompressedHelper $file)
    {
        
        $album=Album::findOrFail($id);
        $com=$file->updateZip($request->zip,$album->compressed_name,'compressed');
        $album->update($com);
        
        return redirect('/admin/albums')->with('success','Successfully updated compressed file');

    }
    public function getCodes($id)
    {
        $album=Album::with(['codes'])->findOrFail($id);
        if($album['codes']){
            // prepare content
    
            $content = "Codes for ".$album->title." \n";
            foreach ($album['codes'] as $key=> $code) {
                $content .= $key+1 ." => ".$code['token'];
                $content .= "\n";
            }

            // file name that will be used in the download
            $fileName = "codes-".$album->title.".txt";

            // use headers in order to generate the download
            $headers = [
            'Content-type' => 'text/plain', 
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            //   'Content-Length' => sizeof($content)
            ];
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        }else{
            return back();
        }
    }
}
