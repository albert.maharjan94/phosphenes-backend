@extends('layouts.admin.app')
@section('title','Album')
@section('albums','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/albums">Album</a></li>
    <li class="active">Create</li>
@endsection
@section('content')
<style>
    .mt-2{
        margin-top: 5px;
    }
</style>
<section class="content">
    @include('layouts.admin.flash-message')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-6">
                    <h3 class="box-title">Add Album</h3>
                    </div>
                </div>
            </div>
        <form role="form" id="p-form" action="/admin/albums" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
            <div class="form-group">
                <label for="description">Album Title</label>
                <input type="text" class="form-control text-area" name="title" value="{{ old('title') }}" placeholder="Enter Title" required>
            </div>
            <div class="form-group">
                <label for="description">Album Description</label>
                <textarea class="form-control text-area" style="max-width:100%;min-width:100%;max-height:300px;" name="description" placeholder="Enter Description" required>{{ old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label for="type">Image Type:</label><br>
                <input type="checkbox" id="image-type" name="type" value="0">
            </div>
            <div class="form-group div-image">
                <label for="image">Image File</label>
                <input type="file" accept="image/*" name="image" id="gallery-image">

                <p class="help-block">Size is limited to 5 MB</p>
            </div>
            <div class="form-group div-url">
                <label for="url">Image Url</label>
                <input class="form-control url" id="gallery-url" name="url" type="text" placeholder="Enter Image Url">
            </div>
           
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            
            <a href="/admin/albums/" class="btn btn-warning">Return</a>
            </div>
        </form>
        </div>
        </div>
    </div>
</section>
@endsection
@push('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap3/bootstrap-switch.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('assets/admin/js/bootstrap-switch.min.js') }}"></script>
<script>
    
    $("#image-type"). prop("checked", false);
    $(document).ready(function(){
        $('input[name="type"]').bootstrapSwitch({
            onText:'File',
            offText:'Url',
            onInit:function(ev){
                url()
                
            },
            onSwitchChange: function(event) {
                if($('#image-type').val()==0){
                    url()
                }else{
                    file()
                }
            }
            
        },false);

    
        function file(){
            $('#image-type').val(0)
            $('.div-image').show(300)
            $('.div-url').hide(300)
            $('#gallery-image').attr('required','required').removeAttr('disabled')
            $('#gallery-url').removeAttr('required','disabled').attr('disabled','disabled')
        }
        function url(){
            $('#image-type').val(1)
            $('.div-image').hide(300)
            $('.div-url').show(300)
            $('#gallery-url').attr('required','required').removeAttr('disabled')
            $('#gallery-image').removeAttr('required').attr('disabled','disabled')
    
        }

        $('#gallery-image').on('change',function(e){
            var file=this.files[0]
            var elem=$(this);
			if(file.size/(1024*1024)>5){
                toastr.error('Image size is limited to 5MB only.');
                elem.val('');
            }
        });

    })

    $('#p-form').on('submit',function(e){
        if($('input[name="title"]').val().length>120)
        {
            toastr.error('Title is limited to 120 words only');
            $('input[name="title"]').focus();
            e.preventDefault();
            $.unblockUI();
            return false;
        }
        if($('input[name="description').length>300){
            toastr.error('Description is limited to 300 words only');
            $('input[name="description').focus();
            e.preventDefault();
            $.unblockUI();
            return false;
        }
    });
    // $('.div-image')


</script>
@endpush