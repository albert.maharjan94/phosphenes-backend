@extends('layouts.frontend.auth')
@section('content')
<div class="limiter">
    <div class="container-login100" style="background-image: url('/assets/frontend/auth/images/bg-01.jpg');">
    <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
        <form class="login100-form validate-form flex-sb flex-w" action="/login" method="POST">
            @csrf
            @include('layouts.frontend.flash-message')
            <span class="login100-form-title p-b-53">
                Login to Continue
            </span>

            <a href="/redirect/facebook" class="btn-face m-b-20">
                <i class="fa fa-facebook-official"></i>
                Facebook
            </a>

            <a href="/redirect/google" class="btn-google m-b-20">
                <img src="{{ asset('assets/frontend/auth/images/icons/icon-google.png') }}" alt="GOOGLE">
                Google
            </a>

            <div class="p-t-31 p-b-9">
                <span class="txt1">
                    Username
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Email is required">
                <input class="input100" type="text" name="email">
                <span class="focus-input100"></span>
            </div>

            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Password
                </span>

                <a href="/forget" class="txt2 bo1 m-l-5">
                    Forgot?
                </a>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <input class="input100" type="password" name="password">
                <span class="focus-input100"></span>
            </div>

            <div class="container-login100-form-btn m-t-17">
                <button class="login100-form-btn">
                    Sign In
                </button>
                <button onclick="location.href='/register'" type="button" class="mt-2 login100-form-btn">
                    Register
                </button>
            </div>


        </form>
    </div>
</div>
</div>

@endsection