@push('scripts')
    
    <div class="modal modal-popup" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom:none">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Download</p>
            </div>
            <div class="modal-footer">
              {{-- <button type="button" class="btn rounded">Save changes</button> --}}
              <button type="button" class="btn rounded btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      @if(Session::has('success'))
        <script type="text/javascript">
            $('.modal-popup').modal('show');
            $('.modal-popup .modal-body p').html('{{ Session::pull('success') }}');
            toastr.success('{{ Session::pull('success') }}')
        </script>
        @endif

        @if(Session::has('error'))
            <script type="text/javascript">
                $('.modal-popup').modal('show');
                $('.modal-popup .modal-body p').html('{{ Session::pull('error') }}');
                toastr.error('{{ Session::pull('error') }}')
            </script>
        @endif
@endpush
@if ($errors->any())
    <div class="alert alert-danger" role="alert" style="width:100%;">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif