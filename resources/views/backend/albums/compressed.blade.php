@extends('layouts.admin.app')
@section('title','Album')
@section('albums','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/albums">Albums</a></li>
    <li class="active">Create Compressed</li>
@endsection
@section('content')
<style>
    .mt-2{
        margin-top: 5px;
    }
</style>
<section class="content">
    @include('layouts.admin.flash-message')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-6">
                    <h3 class="box-title">Add Compressed File</h3>
                    </div>
                </div>
            </div>
        <form role="form" id="p-form" action="/admin/albums/compressed/{{ $album->id }}" method="post" enctype="multipart/form-data">
            @csrf      
            @method('PATCH')
            <div class="box-body">     
                <div class="form-group div-image">
                    <label for="zip">Compressed File</label>
                    <input type="file" id="zip" required accept="zip/*" name="zip" >

                    <p class="help-block">Size is limited to 100 MB</p>
                </div>
           
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                
                <a href="/admin/albums/" class="btn btn-warning">Return</a>
            </div>
        </form>
        </div>
        </div>
    </div>
</section>
@endsection
@push('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap3/bootstrap-switch.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('assets/admin/js/bootstrap-switch.min.js') }}"></script>
<script>
    

        $('#zip').on('change',function(e){
            var file=this.files[0]
            var elem=$(this);
			if(file.size/(1024*1024)>100){
                toastr.error('Audio size is limited to 100MB only.');
                elem.val('');
            }
        });

    })

</script>
@endpush