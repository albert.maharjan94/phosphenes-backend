@extends('layouts.admin.app')
@section('title','Gallery')
@section('galleries','active')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Gallery</li>
@endsection
@section('content')

<div class="modal fade" id="modal-delete" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Delete?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this photo?</p>
        </div>
        <div class="modal-footer">
          <form action="" method="POST">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger">Continue</button>
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal-feat" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Update?</h4>
        </div>
        <div class="modal-body">
          <p>html you sure you want to delete this video?</p>
        </div>
        <div class="modal-footer">
            <form action="" method="POST">
                @csrf
                @method('PATCH')
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Continue</button>
            </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  
  <section class="content">
    @include('layouts.admin.flash-message')
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
              <div class="row">
                  <div class="col-lg-6">
                    <h3 class="box-title">List of Galleries</h3>
                  </div>
                  <div class="col-lg-6 text-right d-flex">
                      <a href="/admin/galleries/create" class="btn btn-primary"><i class="fa fa-plus">  Add</i></a>
                  </div>
              </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Id</th>
                <th>Image</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @foreach($galleries as $key=>$img)
                <tr @if($img->is_featured==1) title="Featured" class="bg-success" @endif>
                    <td>{{ $key+1 }}</td>
                    <td><img class="table-image image image-fluid" src="{{ $img['image']['image_url'] }}"></td>
                    <td>{{ $img->description }}</td>
                    <td>
                        <a class="btn btn-sm btn-icon btn-warning" href="/admin/galleries/{{ $img->id }}/edit"><i class="fa fa-edit"></i> Edit</a>
                        <a class="btn btn-sm btn-icon btn-danger del" data-id="{{ $img->id }}"><i class="fa fa-trash"></i> Delete</a>
                        <a class="btn btn-sm btn-icon {{ $img->is_featured==0 ? 'btn-success' : 'bg-navy' }} feat" 
                          title="{{ $img->is_featured==0 ? 'Make it featured' : 'Removed featured' }}" 
                          data-featured="{{ $img->is_featured }}" data-id="{{ $img->id }}">
                            <i class="fa fa-star"></i>
                          </a>
                    </td>
                </tr>
              @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Id</th>
                <th>Image</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection
@push('styles')
<style>
    .table-image{
       height: 40px;
       width: 40px;
       object-fit:cover; 
    }
</style>

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#example1').DataTable();

    $(document).on('click','.del',function(e){
        e.preventDefault();
        $('#modal-delete form').attr('action','/admin/galleries/'+$(this).data('id'));
        $('#modal-delete').modal('show');
    })

    $(document).on('click','.feat',function(e){
        e.preventDefault();
        var stat=$(this).data('featured');
        if(stat==1){
          $('#modal-feat .modal-body p').html('Do you want to remove the featured for this photo?');
        }else{
          $('#modal-feat .modal-body p').html('Do you want to make this a featured photo?');
        }
        $('#modal-feat form').attr('action','/admin/galleries/featured/'+$(this).data('id'));
        $('#modal-feat').modal('show');
    })
</script>
@endpush