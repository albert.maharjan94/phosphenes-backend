<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Muziq theme</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">

Place favicon.ico and apple-touch-icon.png in the root directory -->
<!-- Lightbox -->
<link rel="stylesheet" href="{{ asset('assets/frontend/styles/magnific/.css') }}" media="screen" />

<link rel="stylesheet" href="{{ asset('assets/frontend/styles/main.css') }}">
<!-- <link rel="stylesheet" href="styles/colors/color-yellow.css">
    <link rel="stylesheet" href="styles/colors/color-lightblue.css">
    <link rel="stylesheet" href="styles/colors/color-purple.css">
    <link rel="stylesheet" href="styles/colors/color-green.css">
    <link rel="stylesheet" href="styles/colors/color-militar.css">
    <link rel="stylesheet" href="styles/colors/color-caqui.css">
    <link rel="stylesheet" href="styles/colors/color-red.css"> -->
{{-- <script src="{{ asset('assets/scripts/vendor/modernizr.js') }}"></script> --}}
@stack('styles')
<style>
  .modal-content{
    background: url('{{ asset("/assets/frontend/images/demo/intro/intro-slide2.jpg") }}') !important; 
    background-size:100% !important; 
    background-position:center;
    color:white;
  }
  .close{
    color: #fff;
    text-shadow: 0 1px 1px #000;
  }
</style>
</head>

<body data-spy="scroll" data-target="#navbar-muziq" data-offset="80">

  <!-- LOADER -->
  <div id="mask">
    <div class="loader">
      <!-- <img src="images/loading.gif" alt='loading'> -->
      <div class="cssload-container">
        <div class="cssload-shaft1"></div>
        <div class="cssload-shaft2"></div>
        <div class="cssload-shaft3"></div>
        <div class="cssload-shaft4"></div>
        <div class="cssload-shaft5"></div>
        <div class="cssload-shaft6"></div>
        <div class="cssload-shaft7"></div>
        <div class="cssload-shaft8"></div>
        <div class="cssload-shaft9"></div>
        <div class="cssload-shaft10"></div>
      </div>
    </div>
  </div>

  <!-- <div class="color-picker">
      <div class="picker-btn"></div>
      <div class="pickerTitle">Style Switcher</div>
      <div class="pwrapper">
        <div class="pickersubTitle"> Color scheme </div>
        <div class="picker-original"></div>
        <div class="picker-yellow"></div>
        <div class="picker-lightblue"></div>
        <div class="picker-purple"></div>
        <div class="picker-green"></div>
        <div class="picker-militar"></div>
        <div class="picker-caqui"></div>
        <div class="picker-red"></div>
        <div class="clear nopick"></div>
      </div>
    </div>  -->

  <!-- HEADER -->
  @include('layouts.frontend.nav')
  @yield('content')

  <!-- FOOTER -->
  <footer>
    <div class="container">
      <p class="copy">© 2020. All Rights Reserved. Developed by Alpas Technology Pvt. Ltd.</p>
      <!--  <ul class="menu-footer">
          <li><a href="#">disclaimer</a></li>
          <li><a href="#">terms & conditions</a></li>
          <li><a href="#">privacy policy</a></li>
        </ul> -->
    </div>
  </footer>

  <script src="{{ asset('assets/frontend/scripts/plugins.js') }}"></script>

  <script src="{{ asset('assets/frontend/scripts/main.js') }}"></script>

  <script src="{{ asset('assets/frontend/scripts/colorpicker.js') }}"></script>

  <script src="{{ asset('assets/frontend/scripts/vendor/bootstrap.js') }}"></script>

  
  @stack('scripts')
</body>

</html>