@extends('layouts.frontend.auth')
@section('content')
<div class="limiter">
    <div class="container-login100" style="background-image: url('/assets/frontend/auth/images/bg-01.jpg');">
    <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
        <form action="/reset-password" method="POST" class="login100-form validate-form flex-sb flex-w">
            @csrf
            
            @include('layouts.frontend.flash-message')
            <span class="login100-form-title p-b-53">
                Request Password
            </span>
            <div class="p-t-13 p-b-9">
                <span class="txt1">
                    Email
                </span>
            </div>
            <div class="wrap-input100 validate-input" data-validate="Email is required">
                <input class="input100" type="email" name="email" required>
                <span class="focus-input100"></span>
            </div>
            <div class="w-100 my-2">
                <a href="/login" class="txt2 bo1 m-l-5">
                    Back to login?
                </a>
                </div>
            <div class="container-login100-form-btn m-t-17">
                <button class="login100-form-btn">
                    Request
                </button>
                
            </div>


        </form>
    </div>
</div>
</div>

@endsection