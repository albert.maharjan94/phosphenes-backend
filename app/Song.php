<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    //
    protected $fillable=[
        'title','lyrics','album_id','is_featured','duration','status','audio_url','audio_name'
    ];

    public function album(){
        return $this->belongsTo(Album::class);
    }
}
