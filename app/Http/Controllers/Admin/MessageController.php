<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    //
    public function index()
    {
        $messages=Message::get();

        return view('backend.messages.index',compact('messages'));
    }

    public function destroy($id)
    {
        $message=Message::findOrFail($id);
        $message->delete();
        return redirect('/admin/messages')->with('success','Successfully deleted.');
    }
}
