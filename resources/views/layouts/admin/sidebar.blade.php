<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="@yield('dashboard')">
      <a href="/admin/dashboard">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
    </li>
    <li class="@yield('albums')">
        <a href="/admin/albums">
            <i class="fa fa-folder-open-o"></i> <span>Albums</span>
        </a>
    </li>
    <li class="@yield('events')">
      <a href="/admin/events">
          <i class="fa fa-calendar-plus-o"></i> <span>Events</span>
      </a>
  </li>
    <li class="@yield('galleries')">
        <a href="/admin/galleries">
            <i class="fa fa-file-image-o"></i> <span>Galleries</span>
        </a>
    </li>
    <li class="@yield('messages')">
        <a href="/admin/messages">
            <i class="fa fa-envelope-o"></i> <span>Messages</span>
        </a>
    </li>
    <li class="@yield('songs')">
        <a href="/admin/songs">
            <i class="fa  fa-music"></i> <span>Songs</span>
        </a>
    </li>
    <li class="@yield('users')">
        <a href="/admin/users">
            <i class="fa fa-user"></i> <span>Users</span>
        </a>
    </li>
    <li class="@yield('videos')">
      <a href="/admin/videos">
          <i class="fa  fa-youtube"></i> <span>Videos</span>
      </a>
  </li>
    {{-- <li class="treeview">
      <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Layout Options</span>
        <span class="pull-right-container">
          <span class="label label-primary pull-right">4</span>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
        <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
        <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
        <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
      </ul>
    </li> --}}
    
  </ul>