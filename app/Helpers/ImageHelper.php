<?php


namespace App\Helpers;

use Illuminate\Support\Facades\File;
use App\Image;

class ImageHelper
{

    public function storeImage($request, $folder,$type)
    {
        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/images/' . $folder), $fileName);

        return $fileName;
    }

    public function deleteImage($folder,$filename)
    {
        if ( File::exists(public_path('storage/images/'.$folder). '/' . $filename)) {
            File::delete(public_path('storage/images/'.$folder) . '/' . $filename);
        }

        return;
    }

    public function updateImage($request, $folder, $oldFile = null,$type)
    {
        $this->deleteImage($folder,$oldFile);

        $fileName =$type.'-'. time() . '.' . $request->getClientOriginalExtension();
        $request->move(public_path('storage/images/' . $folder), $fileName);
        return $fileName;
    }

    public function prepareUpdateImage($request, $model,$folder)
    {
        $img=null;
        try{
            if(isset($request->type)){
                if($request->type==0){
                    if($model['image']['type']==0){
                        $url = $this->updateImage($request->image,$folder,$model->image()->first()->image_name,$folder);
                        $img=new Image(['type'=>0,'image_url'=>asset('storage/images/'.$folder.'/'.$url),'image_name'=>$url]);
                    }else{
                        $url =$this->storeImage($request->image,$folder,$folder);
                        $img=new Image(['type'=>0,'image_url'=>asset('storage/images/'.$folder.'/'.$url),'image_name'=>$url]);
                    }
                    $img=['type'=>0,'image_url'=>asset('storage/images/'.$folder.'/'.$url),'image_name'=>$url];   
                }else{
                    $url=$request->url;
                    $this->deleteImage($folder,$model->image()->first()->image_name);
                    $img=['type'=>1,'image_url'=>$url,'image_name'=>null];
                }
            }else{
                $url=$request->url;
                $this->deleteImage($folder,$model->image()->first()->image_name);
                $img=['type'=>1,'image_url'=>$url,'image_name'=>null];
            }
        }catch(Excepction $e){
            abort(500);
        }
        return $img;
    }

    public function prepareStoreImage($request,$folder)
    {
        $img=null;
        try{
            if(isset($request->type)){
                if($request->type==0){
                    $url =$this->storeImage($request->image,$folder,$folder);
                    $img=new Image(['type'=>0,'image_url'=>asset('storage/images/'.$folder.'/'.$url),'image_name'=>$url]);
                   
                }else{
                    $url=$request->url;
                    $img=new Image(['type'=>1,'image_url'=>$url,'image_name'=>null]);
                   
                }
            }else{
                $url=$request->url;
                
                $img=new Image(['type'=>1,'image_url'=>$url,'image_name'=>null]);
            }
        }catch(Exception $e){
            abort(500);
        }
        return $img;
    }
}