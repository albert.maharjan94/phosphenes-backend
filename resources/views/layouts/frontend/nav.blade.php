<header id="jHeader">
    <nav class="navbar navbar-default" role="navigation">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Desplegar navegación</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- <a class="navbar-brand" href="#home"><img src="images/logo.png') }}" alt="logo"></a> -->
      </div>

      <div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-muziq">
        <ul class="nav navbar-nav navbar-right">
          <li class="@yield('home')"><a href="/#home">Home</a></li>
          <li class="@yield('events')"><a href="/#events">Events</a></li>
          <li><a href="/#members">Members</a></li>
          <li><a href="/#about">About</a></li>
          <li><a href="/#discography">Discography</a></li>
          <!-- <li><a href="#anchor05">Gigs</a></li> -->
          <li><a href="/#gallery">Gallery</a></li>
          <li class="@yield('albums')"><a href="/albums">Album</a></li>
          <!-- <li><a href="shop.html">Shop</a></li> -->
          <li><a href="/#contact">Contact</a></li>
          @if(auth()->user())
          <li><a href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a></li>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
          </form>
          @endif
        </ul>
      </div>

    </nav>
  </header>